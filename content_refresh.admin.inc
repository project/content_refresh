<?php

/**
 * @file
 * Admin settings for the Content Refresh module.
 */

/**
 * Admin settings form for the Content Refresh module
 */
function content_refresh_admin_settings() {
  $form = array();
  $form['content_refresh'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Refresh settings')
  );
  $form['content_refresh']['content_refresh_clear_front_page_cache'] = array(
    '#type' => 'radios',
    '#title' => t('Clear Front Page Cache'),
    '#default_value' => variable_get('content_refresh_clear_front_page_cache', 1),
    '#options' => array(t('No'), t('Yes')),
    '#description' => t('Shall the front page cache be cleared when new promoted content is created or existing promoted content is updated or deleted.')
  );
  $form['content_refresh']['content_refresh_clear_page_cache_on_comment'] = array(
    '#type' => 'radios',
    '#title' => t('Clear Page Cache on Comments'),
    '#default_value' => variable_get('content_refresh_clear_page_cache_on_comment', 1),
    '#options' => array(t('No'), t('Yes')),
    '#description' => t('Shall the page cache be cleared when a user (logged in or not) posts a comment on the page. Only the cache entry for the commented page (node) will be cleared.')
  );
  return system_settings_form($form);
}
