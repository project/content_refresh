
Drupal Content Refresh README
---------------------------------
Author: Ramiro Gómez (http://www.ramiro.org)

This program is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
(see LICENSE.txt)
Requirements
------------
This module requires Drupal 7.x.

Overview:
--------
Content Refresh provides cache clearing functionality for various
content related actions provided page caching is enabled and the
minimun lifetime is greater that 0.
Comment Actions
- The page cache for a page is cleared when a comment is added, edited or deleted.
Front Page Actions
If the Clear Front Page Cache option is set to yes on the module settings page
- The front page cache is cleared when a published promted node is added or deleted.
- The front page cache is cleared when a promted node is unpublished.

Installation and configuration:
------------------------------
Simply extract the download package in your modules directory and 
then enable the module in 'admin/modules'.
For configuration options go to 'admin/config/content/content-refresh'.
